import pandas as pd
from .table_functions import zfill_on_column


def load_crime_table(_path_crime_df, verbose=1):
    """crime_df is produced by 'generate_shocks.ipynb' and contains crime 
    variation magnitudes. this table is used to select shocked municipalities
    """
    
    ## load table
    crime_df = pd.read_csv(_path_crime_df)

    ## parse CVEGEO
    crime_df.rename(columns={'cvegeo': 'CVEGEO'}, inplace=True)
    crime_df = zfill_on_column(crime_df, 'CVEGEO', 5)

    ## parse date ans integer
    crime_df.loc[:, 'date'] = crime_df['date'].str.replace('-', '').astype(int)

    ## keep just homicides
    crime_df = crime_df[crime_df.modality == 'homicides']
    
    if verbose:
        display("crime_df[['date', 'crime_shock_magnitude']].describe()", 
                crime_df[['date', 'crime_shock_magnitude']].describe())
    
    return crime_df


def load_coverage_table(_path_coverage_df, verbose=1):
    """'coverage_df' contains the fraction of clients over estimated official 
    population"""
    
    ## load
    coverage_df = pd.read_csv(_path_coverage_df, index_col=[0,1])

    ## rename index
    coverage_df.index.names = ['CVEGEO', 'Date']

    ## parse CVEGEO
    coverage_df = zfill_on_column(coverage_df, 'CVEGEO', 5)

    if verbose:
        display(coverage_df.head(), f'median of coverage_df: {coverage_df.quantile(0.5)[0]}')
    
    return coverage_df


def load_percentage_change_table(_path_per_change_df, _group_str, verbose=1):
    """'perc_change_df'contains expenditure percentual change on every 
    geospatial entity for a previously specified window length"""
    
    perc_change_df = pd.read_csv(
        _path_per_change_df, header=[0] if _group_str == 'aggregated' else [0, 1], 
        index_col=[0, 1])

    ## rename index
    perc_change_df.index.names = ['CVEGEO', 'Date']

    ## parse CVEGEO
    perc_change_df = zfill_on_column(perc_change_df, 'CVEGEO', 5)

    ## solve columns names bug : On muni results before 13 December 2018
    if _group_str == 'aggregated':
        cols = perc_change_df.columns

    else:

        cols = perc_change_df.columns.get_level_values(0).tolist()

    cols = [col.replace('Tans', 'Trans') for col in cols]
    cols = [col.replace('CC', 'DC') for col in cols]
    # display(cols)

    if _group_str == 'aggregated':
        perc_change_df.columns = cols

    else:
        perc_change_df.rename(columns=dict(
        zip(perc_change_df.columns.get_level_values(0).tolist(), cols)),
                level=0, inplace=True)
    
    if verbose:
        display("perc_change_df.head()", perc_change_df.head())    

    return perc_change_df

def load_percentage_change_dict(_patern_perc_change_tables, _group_list,
                               verbose=1, patern_to_replace='GROUPSTRING'):
    """Loads all the 'perc_change_tables' in a dictionary with subgroup 
    strings as keys. It's used for ploting changes accros different 
    subgroups"""
    
    perc_change_dict = {}

    for g_str in _group_list:
        path_per_change_df = _patern_perc_change_tables.replace(
            patern_to_replace, g_str)
        
        if verbose:
            display(g_str)
        perc_change_dict[g_str] = load_percentage_change_table(
            path_per_change_df, g_str, verbose=verbose)

    return perc_change_dict