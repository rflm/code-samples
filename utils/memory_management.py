import os
import psutil

# functions
def report_memory(custom_str="", report_pid=False):
    """Reports current use of memory and Process ID if 'report_pid'
    is set to True"""
    
    # get pid
    process = psutil.Process(os.getpid())
    print(f'Process ID: {os.getpid()}') if report_pid else None
    
    # display current use of memory
    if custom_str:
        custom_str = ' ' + custom_str
    print(f'memory used{custom_str}: {process.memory_info().rss:,}')