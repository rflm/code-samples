import pandas as pd
from .table_functions import map_on_column, divide_multiindexed_dfs,\
slice_on_column

class PercentageChangeCalculator():
    """Class used for aggregating the 'exp_agg' table on fixed windows.
    This is meant to be useful for calculating the percentual changes 
    between each pair of consecutive windows. The parameters which 
    defines how these windows are structured is whether 'date_slice_tuples'
    or 'date_dict_map'
    """
    
    def __init__(self, header, index,  dtype, date_level_name,
                 date_slice_tuples=None, date_dict_map=None,
                 verbose=1):

        # header on csv: used on 'read_table' method
        assert isinstance(header, (int, list))
        self.header = header

        # index on csv: used on 'read_table' mehod
        assert isinstance(index, (int, list))
        self.index = index

        # dtype of csv: used on 'read_table' mehod
        assert isinstance(dtype, (dict, str, type))
        self.dtype = dtype            

        # date column on DF: used on 'treat_date' method
        assert isinstance(date_level_name, (str, int))
        self.date_level_name = date_level_name
        
        # date_slice_tuples: used on 'treat_date' method
        if date_slice_tuples:
            
            assert isinstance(date_slice_tuples, tuple)
            self.date_slice_tuples = date_slice_tuples
        
        else:
            
            self.date_slice_tuples = None
            
        # date_dict_map: used on 'treat_date' method
        if date_dict_map:
            
            assert isinstance(date_dict_map, dict)
            self.date_dict_map = date_dict_map
            
        else:
            
            self.date_dict_map = None
            
        # display parameters of the object
        if verbose:
            display(vars(self))
     
            
    def read_table(self, p_csv):
        """Loads the table using the parameters of the 
        'PercentualChangeCalculator'object."""
        
        #df = 
        return pd.read_csv(p_csv, header=self.header, index_col=self.index,
                         dtype=self.dtype)
    

    def treat_date(self, df):
        """Only slice and map tratment are implemented. It is dependen 
        on functions 'slice_on_column' and 'map_on_column' which should
        be located on 'utils.table_functions' package"""
        
        # slice treatment
        if self.date_slice_tuples:
            df = slice_on_column(df, self.date_level_name, self.date_slice_tuples)
        
        # mapping with dictionary tratment
        elif self.date_dict_map:
            df = map_on_column(df, self.date_level_name, self.date_dict_map)
        
        return df
           

    def aggregate_on_dates(self, df):
        """This method suposes the dates are part of the index"""
        
        # groupby - I MIGHT NEED TO TRY / CATCH FOR OTHER CASES
        agg_df = df.groupby(level=df.index.names).sum()
        
        return agg_df
    
    
    def calculate_perc_changes(self, df):
        """It calculates percentual changes assuming 'self.date_level_name'
        alphabetic order correspond to chronological order"""
        
        # initialize denominator
        _den = df.copy()

        # get date mapper
        date_idx = sorted(df.index.get_level_values(self.date_level_name
                                                   ).unique().tolist())
        idx_den = date_idx[1:] + ['9999']
        date_den_dict_map = dict(zip(date_idx, idx_den))
        
        # map date
        _den = map_on_column(_den, 'Date', date_den_dict_map)
        
        # divide
        _div = divide_multiindexed_dfs(df, _den, 0)

        # transform to percentage change
        return  (_div - 1) * 100
    

    def get_aggregated_dates(self, p_csv, verbose=1):
        """Agglomerates all the methods which allow to return a df with
        aggregated dates. 
        """
        
        # read the table
        if isinstance(p_csv, str):
            df = self.read_table(p_csv)
        elif isinstance(p_csv, pd.DataFrame):
            df = p_csv.copy()
        
        # display('read done', df.head())
        #print(df.index.names.tolist())
        
        # window tratment
        df = self.treat_date(df)
        # display('date treatment', df.head(13))
        
        # return df
        
        # aggregate windows
        agg_df = self.aggregate_on_dates(df)
        if verbose:
            display('aggregate on dates', agg_df.head())
        
        return agg_df
        
        
    def get_percentual_changes(self, agg_df):
        """This method call all the other methods of the class to return
        the DF with aggregates on each of the columns which are not part 
        of the index"""
        
#         # read the table
#         if isinstance(p_csv, str):
#             df = self.read_table(p_csv)
#         elif isinstance(p_csv, pd.DataFrame):
#             df = p_csv
            
#         display('read done', df.head())
#         #print(df.index.names.tolist())
        
#         # window tratment
#         df = self.treat_date(df)
#         display('date treatment', df.head())
        
#         # return df
        
#         # aggregate windows
#         agg_df = self.aggregate_on_dates(df)
#         display('aggregate on dates', agg_df.head())

        
        # get percentual change
        perc_change = self.calculate_perc_changes(agg_df)
        display('perc_change calculated', perc_change.head(), perc_change.shape)
        
        return perc_change

# testing space
# test = PercentageChangeCalculator(
#     [0, 1], [0,1], float, 'Date', date_slice_tuples=(4, ))   

# perc_change_df = test.get_percentual_changes(
#     '../../../intermidiate_results/expenditure_aggregates_tables/Muni_\
# TotalExp_TotalExp-20181125.csv')