import pandas as pd
import numpy as np

def zfill_on_column(df, col_name, n_zeros):
    """Parses the column 'col_name' of DataFrame 'df' as string with 'n_zeros'
    zeros on the left, even if this column ''"""
    try:
        
        # normalize column which will be merged - adding zeros to the left
        df.loc[:, col_name] = df[col_name].astype(str).str.zfill(n_zeros)
    
    # in case of KeyError seach on Index
    except KeyError:
        
        # store column names of index
        # display(df.index.names)
        level_idx = df.index.names.index(col_name)
        
        # build the array 
        midx_arrays = [
            df.index.get_level_values(i).values for i in range(
                len(df.index.names)) if i != level_idx 
        ]
        midx_arrays.insert(level_idx, 
                           df.index.get_level_values(level_idx).astype(
                           str).str.zfill(n_zeros).values)
        # set parsed index
        df.index = pd.MultiIndex.from_arrays(
            midx_arrays, names=df.index.names)
        
    return df

def map_on_column(df, col_name, dict_map):
    """Applies pd.Series.map in a column or part of the index
    """
    
    assert isinstance(dict_map, dict), "'dict_map' has to be of type dict"
    
    
    try:
        
        # normalize column which will be merged - adding zeros to the left
        df.loc[:, col_name] = df[col_name].map(dict_map)
    
    # in case of KeyError seach on Index
    except KeyError:
        
        # store column names of index
        # display(df.index.names)
        if isinstance(col_name, str):
            level_idx = df.index.names.index(col_name)
        elif isinstance(col_name, int):
            level_idx = col_name
        else:
            raise TypeError("If colname is refering to a level name on Index,\
 it must be a 'str' ot 'int'")
        
        # build the array 
        midx_arrays = [
            df.index.get_level_values(i).values for i in range(
                len(df.index.names)) if i != level_idx 
        ]
        midx_arrays.insert(level_idx, 
                           df.index.get_level_values(level_idx).map(
                               dict_map))
        # set parsed index
        df.index = pd.MultiIndex.from_arrays(
            midx_arrays, names=df.index.names)
        
    return df
    
def divide_multiindexed_dfs(_df1, _df2, axis, drop=True, inf_to_nan=True):

    # copy DF's
    df1 = _df1.copy()
    df2 = _df2.copy()
    
    # transform pd.MultiIndex to tuples
    df1.index = df1.index.to_series()
    df2.index = df2.index.to_series()
    
    # divide
    # display(df1, df2)
    res = df1.divide(df2, axis=axis)
    # display(res)

    # drop entirely missing rows
    if drop:
        res = res[~ res.isnull().all(1)]

    # replace infinites for missing
    if inf_to_nan:
        res.replace(np.inf, np.nan, inplace=True)
    
    # transform tuples in pd.MuliIndex
    res.index = pd.MultiIndex.from_tuples(res.index)
    
    return res

def slice_on_column(df, col_name, slice_tuple):
    """Parses the column 'col_name' of DataFrame 'df' as string and keeping
    the slice of it defined by 'slice_tuple'. the method works even if the column
    named 'col_name' is part of the index. As a convention 'slice_tuple'
    has to have length 1 or 2. In case it has lenght 1 the slice will be 
    taken from position 0 to the integer contained in slice_tuple.
    If slice has lenght 2 the second component can be None, meaning 
    the slice nedd to be taken tothe end. As an example:
    - (3, None): the slice will be taken from position 3 to the end
    """
    
    # parse sice tuple
    if len(slice_tuple) == 2:
        _start, _end = slice_tuple
    
    elif len(slice_tuple) == 1:
        _start = None
        _end = slice_tuple[0]
        
    else:
        raise ValueError("'slice_tuple' can have lenght 1 or 2")
    print('start and end of slice:', (_start, _end))
    
    try:
        
        # normalize column which will be merged - adding zeros to the left
        df.loc[:, col_name] = df[col_name].astype(str).str.slice(_start, _end)
    
    # in case of KeyError seach on Index
    except KeyError:
        
        # store column names of index
        # display(df.index.names)
        level_idx = df.index.names.index(col_name)
        
        # build the array 
        midx_arrays = [
            df.index.get_level_values(i).values for i in range(
                len(df.index.names)) if i != level_idx 
        ]
        midx_arrays.insert(level_idx, 
                           df.index.get_level_values(level_idx).astype(
                           str).str.slice(_start, _end).values)
        # set parsed index
        df.index = pd.MultiIndex.from_arrays(
            midx_arrays, names=df.index.names)
        
    return df

def drop_rows_on_column_value(df, column_name, value, drop_col=False):
    """Drops rows which have an specific value on column 'column_name'"""
    
    # get index of eliminable columns
    idx_bools = df.index[df[column_name] == value]
    
    # drop rows 
    df = df.drop(idx_bools, axis=0)
    
    # drop used column
    if drop_col:
        df = df.drop(column_name, axis=1)
    
    return df
