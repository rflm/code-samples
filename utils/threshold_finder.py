# dependences
import pickle
import json
import pandas as pd
import seaborn as sns
import numpy as np
from scipy import stats


# performance function used in the class below
def get_mwu_pvalue(shock_values, control_values, alternative=None):
    stat, pvalue = stats.mannwhitneyu(shock_values, control_values, 
                                     alternative=alternative)
    return (stat, pvalue)

def get_ttest_pvalue(shock_values, control_values, equal_var):
    stat, pvalue = stats.ttest_ind(shock_values, control_values, 
                                   equal_var=equal_var)
    return (stat, pvalue)

def get_cbars_difference(shock_values, control_values):
    shock_interval = sns.utils.ci(sns.algorithms.bootstrap(shock_values, n_boot=1000))
    control_interval = sns.utils.ci(sns.algorithms.bootstrap(control_values, n_boot=1000))
    return min(control_interval.min() - shock_interval.max(),
               control_interval.max() - shock_interval.min(),
               control_interval.max() - shock_interval.max(),
               control_interval.min() - shock_interval.min())


class ThresholdFinder():
    """It excecutes a random search on the space of parameters defined by 
    'threshold_bounds_list', and is capable of calculating the performance 
    measures for each n-tuple of parameters. These results are stored as a 
    DataFrame on the attribute 'threshold_performance_df'"""
    
    def __init__(self, coverage_df, repre_max_left_out, window_width, window_list,
                 window_list_tuples, crime_df, max_top_quantile, verbose=1):
        
        # params of random search
        assert isinstance(repre_max_left_out, float)
        self.repre_max_left_out = repre_max_left_out
        
        # set threshold list on representativity
        assert isinstance(coverage_df, pd.DataFrame)
        assert isinstance(crime_df, pd.DataFrame)
        crime_magnitudes = crime_df.crime_shock_magnitude
        self.threshold_bounds_list = [
            (0.0, coverage_df.quantile(self.repre_max_left_out)[0]),
            (#stats.percentileofscore(crime_magnitudes, 100) / 100, 
            2.0/3,
             max_top_quantile)]
        
        # store window_width: used on crime_df filtering
        assert isinstance(window_width, int)
        self.window_width = window_width
        
        # times in which the analysis is carriesd out
        assert isinstance(window_list, list)
        self.window_list = window_list
        
        # to filter crime table
        assert isinstance(window_list_tuples, list)
        self.window_list_tuples = window_list_tuples
        
        # filename where results will be saved (as 'random_search_thresholds'
        # is expected to be set up
        # externally, ie. none method set up this attribut
        self.path_threshold_search_table = None
        self.study_col = None
        
        # results columns
        self.threshold_search_df_cols = [
            'NTry', 'Window', 'Repre_Min', 'Perc_Thresh', 'Crime_Shock_Def', 'NShocked',
            'NControl', 'TTest_Pval', 'Welch_Pval', 'MWU_Pval', 'CI_Distance']
        # pd.MultiIndex.from_product(
        #     [['M', 'F'], ['welch_pval', 'mwu_pval', 'ci_distance']])
        
        # display 
        if verbose:
            display('Parameters which define the threshold search:', 
                    vars(self))
    
    
    
    def get_random_vector(self):
        """An iterable of tuples ('min', 'max') float pairs is expected on 
        attribute  
        The interval has uniform probability"""


        ret = np.empty((len(self.threshold_bounds_list),))
        # _thresholds = self
        for i, (_min, _max) in enumerate(self.threshold_bounds_list):
            ret[i] = np.random.uniform(_min, _max)

        return ret
    
    def get_admissible_representativity_entities(
        self, coverage_df, window, repre_min, repre_max=1.0):
        """Gets the spatials entiities where client representativity
        is in ['repre_min', 'repre_max'] during 'window' period"""
        
        # for ease transform to Series
        repre_srs = coverage_df.xs(window, axis=0, level='Date').iloc[:, 0]
        # display(repre_srs)
        
        # point to admisible geospatial entities
        bools = (repre_srs >= repre_min) & (repre_srs <= repre_max)
        
        return repre_srs.index[bools]
    
    def get_shocked_control_entities(self, _crime_df, window_idx, top_perc, 
                             entities_col='CVEGEO',
                             max_on='crime_shock_magnitude'):
        """Gets the spatial entities which are shocked during 'window'
        period according to 'top_perc'"""
        
        # work on copy
        crime_df = _crime_df.copy()
        # display('unfiltered crime df', crime_df.head())
        
        
        # filter date
        _min, _max = self.window_list_tuples[window_idx]
        bools_date = (crime_df.date >= _min) & (crime_df.date <= _max) 
        crime_df = crime_df[bools_date]
        # display('date filter', crime_df.date.describe())

        # filter on max nan's on both windows
        bools_nans = (crime_df.window_nans <= .25 * self.window_width) & (
            crime_df.window_r0_nans <= 0.25 * self.window_width)
        crime_df = crime_df[bools_nans]
        # display('nans filter', crime_df.head())

        # get max per spatial entity
        crime_max_srs = crime_df.groupby(entities_col)[max_on].max()
        # display('crime_max_srs', crime_max_srs)
        crime_shock_threshold = crime_max_srs.quantile(top_perc) # -
        # if crime_shock_threshold <= 100.0:
        #     raise
        bools_shock = crime_max_srs >= crime_shock_threshold
        
        return crime_max_srs.index[bools_shock], crime_max_srs.index[
            ~ bools_shock], crime_shock_threshold
    
    
    
    def get_performance(self, shocked_array, control_array):
        
        ttest_stat, ttest_pval = get_ttest_pvalue(shocked_array, 
                                                  control_array, True)
        welch_stat, welch_pval = get_ttest_pvalue(shocked_array, 
                                                  control_array, False)
        mwu_stat, mwu_pval = get_mwu_pvalue(shocked_array, control_array)
        ci_diff = get_cbars_difference(shocked_array, control_array)
        
        return [ttest_pval, welch_pval, mwu_pval, ci_diff]
    
    def get_shocked_and_control_arrays(
        self, repre_min, top_perc, window_idx, coverage_df, crime_df, 
        _perc_change_df, study_col, entities_col):
        
        # parse window_idx
        window = self.window_list[window_idx]
        
        # filter representativity: needs the first four digits to 
        # correspond to a year str which need to be trnsformed to integer 
        repre_ent = self.get_admissible_representativity_entities(
            coverage_df, int(str(window)[:4]), repre_min)
        # display('representative entities', repre_ent, 'entities column',
        #         _perc_change_df.reset_index()[entities_col].head())

        # filter crime
        shocked_ent, control_ent, cs_thresh = self.get_shocked_control_entities(
            crime_df, window_idx, top_perc)
        # display('shocked', shocked_ent, 'control', control_ent)
        
        # intersection of filtered munis
        shocked_ent = shocked_ent[shocked_ent.isin(repre_ent)]
        control_ent = control_ent[control_ent.isin(repre_ent)]
        # display('shocked and representative', shocked_ent, 
        #         'control and representative', control_ent)

        # filter perc_change_df: 
        perc_change_df = _perc_change_df.reset_index()
        # display('perc_change_df', perc_change_df.head(), 'bools_1', perc_change_df[entities_col].isin(
        #     shocked_ent).sum(), 'bools_2', perc_change_df.Date.describe(), window)
        ## shocked entities
        bools_shocked_pchange = perc_change_df[entities_col].isin(
            shocked_ent) & (perc_change_df.Date == window)
        w_shocked_perc_change = perc_change_df[bools_shocked_pchange]
        ## control entities
        bools_control_pchange = perc_change_df[entities_col].isin(
            control_ent) & (perc_change_df.Date == window)
        w_control_perc_change = perc_change_df[bools_control_pchange]

        shock_array = w_shocked_perc_change.loc[:, study_col].dropna(
        ).values
        control_array = w_control_perc_change.loc[:, study_col].dropna(
        ).values
        
        return shock_array, control_array, cs_thresh
        
    
    def random_search_thresholds(self, ntries, coverage_df, crime_df, _perc_change_df,
                                 study_col, entities_col='CVEGEO'):
        
        # keep track of studied column
        self.study_col = study_col
        
        threshold_search_df = pd.DataFrame(
            np.ones((ntries * len(self.window_list),  # 2 for gender
                     len(self.threshold_search_df_cols))) * np.nan,
            columns=self.threshold_search_df_cols)
        
        for i in range(ntries):
            # get random vector
            repre_min, top_perc = self.get_random_vector()
            #print(f'random vetor: {(repre_min, top_perc)}')

            for w_idx, window in enumerate(self.window_list):
                
                # get shock and control arrays
                shock_array, control_array, cs_thresh = self.get_shocked_and_control_arrays(
                    repre_min, top_perc, w_idx, coverage_df, crime_df, 
                    _perc_change_df, study_col, entities_col)  
                # display('shock array', shock_array, 'control array', control_array)
                
                # evaluate performace
                performance_list = self.get_performance(shock_array, control_array)
                
                # store on DF
                threshold_search_df.iloc[
                    i * len(self.window_list) + w_idx, :
                ] = [i, window, repre_min, top_perc, cs_thresh, len(shock_array),
                     len(control_array)] + performance_list
            
            # monitor
            if i % 100 == 0: 
                print(f'try {i} succeded')
                
        return threshold_search_df
                
    def to_json(self, filename):
        """Writes a copy of the object in JSON format (human-readable)"""
        
        with open(f'{filename}.json', 'w') as file:
            file.write(json.dumps(vars(self), sort_keys=True, indent=4))
            
    def to_pickle(self, filename):
        """Writes a copy of the object in binary format (machine-readable)"""
        
        pickle.dump(self, open(f'{filename}.pk', 'wb'))
        
    def save(self, filename):
        """Calls all the saving methods of the class"""
        
        self.to_json(filename)
        self.to_pickle(filename)

# test space
# threshold_finder = ThresholdFinder(
#     coverage_df, repre_max_left_out, window_width, window_list, 
#     window_list_tuples, crime_df, max_top_quantile)
# threshold_finder.random_search_thresholds(
#     10, coverage_df, crime_df, perc_change_df, col_interest)
    